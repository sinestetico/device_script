#define NUM_SENSORS 15

int values[NUM_SENSORS];

// init analog input pins
void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
}

// read the first 15 sensors (number 0 to 14)
void loop() {
  Serial.print("[");
  for(int i=0; i<NUM_SENSORS; i++) {
    int value = analogRead(i);
    delay(5);
    value = analogRead(i);
    
    Serial.print(value);
    if(i < NUM_SENSORS-1) {
      Serial.print(",");
    }
  }
  Serial.println("]");
}

/*
// read the last 15 sensors (number 15 to 29)
void loop() {
  Serial.print("[");
  for(int i=NUM_SENSORS-1; i>-1; i--) {
    int value = analogRead(i);
    delay(5);
    value = analogRead(i);
    
    Serial.print(value);
    if(i > 0) {
      Serial.print(",");
    }
  }
  Serial.println("]");
}
*/

