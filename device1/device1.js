var socketio = require('socket.io-client');
var serialport = require('serialport-v4');
var Promise = require("bluebird");
var peakFinder = require("peak-finding");

setTimeout(function() {

        var port = new serialport('/dev/ttyACM0', {
	  baudrate: 115200,
	  buffer: 1000,
	  parser: new serialport.parsers.readline('\r\n')
	});
    
	// init data
	var NUM_SENSORS = 15;
	var NUM_SAMPLES = 100;
	var samples = new Array();
	var lastBeat = new Array();
	var pos = new Array();
	var enabled = {};
	var socket = socketio('http://localhost:4001');

	// init the array for a sensor
	for(var i=0; i<NUM_SENSORS; i++) {
	  samples.push(new Array());
	  lastBeat.push(Date.now());
	  pos.push(0);
	  enabled[i] = true;
	  for(var j=0; j<NUM_SAMPLES; j++)
	    samples[i].push(0);
	}

	// used to filter data updates
	socket.on('config', function(mess) {
	  enabled = mess.pos;
	  //console.log(enabled);
	});

	// console.log(samples);
	// console.log(lastBeat);
	// console.log(pos);

	// return the max value
	function computeMax(values) {
	  return Math.max.apply(null, values);
	}
	// compute the average
	function computeAvg(values) {
	  return values.reduce((a,b) => a+b, 0) / values.length;
	};
	// compute the variance
	function computeVar(avg, values) {
	  var diff = values.map(function(v) {
	    return Math.pow(v - avg, 2);
	  });
	  var sum = diff.reduce((a,b) => a+b, 0.0);
	  return sum / (values.length + 0.0);
	};
	// detect the beat for each sensor
	function detectBeat(value, index) {
	  // manage the sensor data
	  var avgVal = computeAvg(samples[index]);
	  var varVal = computeVar(avgVal, samples[index]);
	  //console.log(index, value, avgVal, varVal);
	  // too much variance == sensor not connected
	  if (varVal < 1 || varVal > 100) {
	    return false;
	  }
	  //console.log(index, value, avgVal, varVal);
	  // find the peak in the signal
	  var maxVal = computeMax(samples[index]);
	  var diff = (maxVal - avgVal) / 2.0;
	  if (value >= (avgVal+diff)) {
	    // var now = Date.now();
	    // var BPM = 60000 / (now - lastBeat[index]);
	    // console.log(BPM);
	    console.log(index, value, avgVal, varVal);
	    return true;
	  }
	  return false;
	};

	// inizializza la lettura dalla porta seriale
	port.on('open', function() {
	  // ripulisce la porta seriale
	    // gestisce la ricezione dei dati
	    port.on('data', function(data) {
	      try {
		// converte i dati nel buffer
		var values = JSON.parse(data);
		// aggiunge i dati ai buffer
		values.map(function(v, i) {
		  if (!enabled[i])
		    return;
		  //console.log(v, i);
		  samples[i][pos[i]] = v;
		  pos[i] = (pos[i]+1) % NUM_SAMPLES;
		  //var res = detectBeat(v, i);
		  // evita il conteggio doppio
		  if (lastBeat[i]+100 < Date.now()) {
		    // console.log(i, v, 'time ok');
		    var res = detectBeat(v, i);
		    if (res) {
		      console.log(i, '-', 'Beat detected');
		      socket.emit('pulse', {deviceId: i});
		      lastBeat[i] = Date.now();
		    }
		  }
		});
	      }
	      catch(err) {
		console.error(err);
	      }
	    });
	});

}, 5000);
